import os
import json

settings = None


def load_settings():
    global settings
    with open(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                           'settings.json')) as settingsFile:
        settings = json.load(settingsFile)


# We call the function to load the settings.json file
load_settings()
