# This is our first Feature file
@login
Feature: Feature example to test our web

  Background: Navigate to Login Page
    Given the user is in the login page

  @successful
  Scenario: Login success
    When the user inserts the login credentials
    And the user clicks the login button
    Then close browser

  @unsuccessful
  Scenario: Login Unsuccessful by credentials
    When the user inserts the wrong login credentials
    And the user clicks the login button
    Then an error message "Invalid credentials" is shown
    And close browser

