Feature: Login in our website
  Background: Navigate to Login Page
    Given the user is in the login page

  Scenario Outline: Login with multiple users
    When the user inserts "<username>" in username field
    And the user inserts "<password>" in password field
    And the user clicks the login button
    Then close browser

    Examples: Users
      | username | password |
      | Admin    | admin123 |
      | Nimda    | 321nimda |