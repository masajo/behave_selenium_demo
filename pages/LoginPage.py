from configurations.config import settings
from framework.WebFramework import webFramework


class LoginPage:
    instance = None

    # Constructor to initialize the driver from our framework
    def __init__(self):
        self.driver = webFramework.get_driver()

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = LoginPage()
        return cls.instance

    # Implement insertion of credentials
    def insert_credentials(self):
        # Insert username
        self.driver.find_element_by_xpath('//*[@id="txtUsername"]').send_keys(str(settings['username']))
        self.driver.find_element_by_xpath('//*[@id="txtPassword"]').send_keys(str(settings['password']))

    # Implement insertion of credentials
    def insert_wrong_credentials(self):
        # Insert username
        self.driver.find_element_by_xpath('//*[@id="txtUsername"]').send_keys(str(settings['wrong_username']))
        self.driver.find_element_by_xpath('//*[@id="txtPassword"]').send_keys(str(settings['wrong_password']))

    # Verify the existence of an error message
    def verify_error_message(self):
        assert 'Invalid credentials' in self.driver.find_element_by_xpath('//*[@id="spanMessage"]').text, \
            f'Error Message was not found in the page {self.driver.current_url}'

    # Implement click the login button
    def click_login_button(self):
        self.driver.find_element_by_xpath('//*[@id="btnLogin"]').click()


loginPage = LoginPage.get_instance()
