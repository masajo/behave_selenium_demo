import logging
import logging.handlers


class Logger:

    @staticmethod
    def logGenerator():
        logging.basicConfig(
            filename="logs/tests.log",
            format='%(asctime)s - %(messages)s',
            filemode='w',
            datefmt='%d/%b/%y %H:%M:%S'
        )

        r_file = logging.handlers.RotatingFileHandler(
            'logs/tests.log',
            backupCount=3,
            maxBytes=1024 * 1024 * 20
        )

        logger = logging.getLogger()
        logger.addHandler(r_file)
        logger.setLevel(logging.INFO)

        return logger
