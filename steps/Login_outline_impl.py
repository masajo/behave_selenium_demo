from behave import when, step
from utilities.Logger import Logger

logger = Logger.logGenerator()


@step(u'Step no definido bajo un tipo concreto')
def mifuncion(context):
    print('Ejemplo de Step sin declarar el tipo concreto')


@when(u'the user writes his "{username}"')
@when(u'the user inserts "{username}" in username field')
def step_impl(context, username):
    context.driver.find_element_by_xpath('//*[@id="txtUsername"]').send_keys(username)


@when(u'the user inserts "{password}" in password field')
def step_impl(context, password):
    context.driver.find_element_by_xpath('//*[@id="txtPassword"]').send_keys(password)
