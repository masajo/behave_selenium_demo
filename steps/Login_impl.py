import time

from behave import given, when, then
from framework.WebFramework import webFramework
from pages.LoginPage import loginPage
from configurations.config import settings
from utilities.Logger import Logger

# Generate our Logger
logger = Logger.logGenerator()

# Implement the Given step
@given(u'the user is in the login page')
def step_impl(context):
    logger.info('--- BROWSER LOADS AND NAVIGATES TO LOGIN URL --- ')
    webFramework.load_website()
    logger.info(f'--- BROWSER NAVIGATES CORRECTLY --- ')


# Implement the When Step
@when(u'the user inserts the login credentials')
def step_impl(context):
    logger.info('--- USER ENTERS CORRECT LOGIN CREDENTIALS --- ')
    loginPage.insert_credentials()
    logger.info('--- USER ENTERED CORRECT LOGIN CREDENTIALS --- ')



@when(u'the user inserts the wrong login credentials')
def step_impl(context):
    logger.info('--- USER ENTERS INCORRECT LOGIN CREDENTIALS --- ')
    loginPage.insert_wrong_credentials()
    logger.info('--- USER ENTERED INCORRECT LOGIN CREDENTIALS --- ')


# Implement the When And step
@when(u'the user clicks the login button')
def step_impl(context):
    loginPage.click_login_button()
    logger.info('--- USER CLICKS LOGIN BUTTON --- ')



@then(u'an error message "Invalid credentials" is shown')
def step_impl(context):
    logger.info('--- VERIFYING ERROR MESSAGE --- ')
    time.sleep(5)
    loginPage.verify_error_message()
    logger.info('--- VERIFIED ERROR MESSAGE --- ')


# Implement the Then step
@then(u'close browser')
def step_impl(context):
    logger.info('--- BROWSER CLOSING --- ')
    webFramework.close_browser()
    logger.info('--- BROWSER CLOSED --- ')
