from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options
from configurations.config import settings
from urllib.parse import urljoin


# Framework class that contains main actions and controls the webdriver
class WebFramework:
    instance = None

    # Constructor
    def __init__(self):
        # Evaluate the webDriver by the browser set in settings.json
        if str(settings['browser']).lower() == 'chrome':
            chrome_options = Options()
            chrome_options.add_argument("--headless")
            self.driver = webdriver.Chrome(executable_path='/usr/local/bin/chromedriver', chrome_options=chrome_options)
            self.driver.implicitly_wait(30)
            self.driver.set_page_load_timeout(30)
            self.driver.maximize_window()

        elif str(settings['browser']).lower() == 'firefox':
            firefox_options = Options()
            firefox_options.add_argument("--headless")
            self.driver = webdriver.Firefox()

            self.driver = webdriver.Chrome(executable_path='/usr/local/bin/geckodriver', firefox_options=firefox_options)
            self.driver.implicitly_wait(30)
            self.driver.set_page_load_timeout(30)
            self.driver.maximize_window()

        else:
            chrome_options = Options()
            chrome_options.add_argument("--headless")
            self.driver = webdriver.Chrome(executable_path='/usr/local/bin/chromedriver', chrome_options=chrome_options)
            self.driver.implicitly_wait(30)
            self.driver.set_page_load_timeout(30)
            self.driver.maximize_window()

    @classmethod
    def get_instance(cls):
        if cls.instance is None:
            cls.instance = WebFramework()
        return cls.instance

    def get_driver(self):
        return self.driver

    # Load the WebSite from the URL in Settings
    def load_website(self):
        self.driver.get(settings['url'])

    # Navigate to another URL
    def navigate_page(self, endpoint: str):
        self.driver.get(urljoin(settings['url'], endpoint.lower()))

    # Close the Browser
    def close_browser(self):
        self.driver.close()

    # Verify that a component exists in the current page
    def verify_component_by_tag(self, component):
        assert component in self.driver.find_element_by_tag_name('body').text, \
            f'Component {component} was not found in the page {self.driver.current_url}'


webFramework = WebFramework.get_instance()
